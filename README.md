[![CircleCI](https://circleci.com/bb/lnovara/saccharomyces-cerevisiae-docker.svg?style=shield)](https://circleci.com/bb/lnovara/saccharomyces-cerevisiae-docker)
[![Docker Build Status](https://img.shields.io/docker/build/lnovara/saccharomyces-cerevisiae.svg)](https://hub.docker.com/r/lnovara/saccharomyces-cerevisiae/builds/)

# saccharomyces-cerevisiae Docker image #
Docker image based on
[ngs-tools-docker](https://bitbucket.org/lnovara/ngs-tools-docker) containing
Saccharomyces cerevisiae reference files.
